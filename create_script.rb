#!/usr/bin/env ruby

require 'date'
require 'erb'
require 'optparse'

options = {}
options[:stories] = []

OptionParser.new do |opts|
  opts.banner = "Usage: create_script.rb [options]"

  opts.on('--date YYYY-MM-DD', "Select the date of this demo") do |date|
    if date == "tomorrow"
      options[:date] = (Date.today + 1).to_datetime
    else
      options[:date] = DateTime.parse(date) || DateTime.now
    end
  end

  opts.on('--story "TITLE | TYPES"', "Add a story to this script. TYPES is 'a' for Android and/or 'w' for Web") do |story|
    options[:stories] << story
  end
end.parse!

# Vars for binding

date = options[:date].strftime('%Y-%m-%d')

stories = options[:stories].map { |story_input|
  title, types = story_input.split('|')
  {
    title: title,
    android: types.downcase.include?('a'),
    web: types.downcase.include?('w'),
  }
}

# / Vars

file = File.read('./template.md.erb')
new_script = ERB.new(file, nil, '-').result(binding)

name = "demo-#{date}.md"

raise "Demo script already exists" if File.exists?(name)

File.open(name, 'w') { |f|
  f.write(new_script)
}
